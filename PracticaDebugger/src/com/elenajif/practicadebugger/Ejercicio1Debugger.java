package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio1Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar instruccion a
		 * instruccion (step into) analizando el contenido de las variables
		 * 
		 */

		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;

		cantidadVocales = 0;
		cantidadCifras = 0;

		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();

		/*
		 * El error sucede debido a la condicion del bucle for, ya que esta indica que
		 * seguira hasta que la i valga menos o sea igual a la longitud de la cadena. El
		 * metodo de la �cadena.charAt()� cuenta los caracteres desde el 0 pero la
		 * longitud cuenta desde el 1, por lo que por ejemplo en la cadena �hola� la
		 * longitud es 4 pero el ultimo caracter seria 3, el error sucede porque intenta
		 * coger un caracter mas alla de la longitud de la cadena. Por lo que la
		 * solucion seria dejar el bucle de la siguiente manera: for (int i = 0; i <
		 * cadenaLeida.length(); i++)
		 */

		for (int i = 0; i <= cadenaLeida.length(); i++) {
			caracter = cadenaLeida.charAt(i);
			if (caracter == 'a' || caracter == 'e' || caracter == 'i' || caracter == 'o' || caracter == 'u') {
				cantidadVocales++;
			}
			if (caracter >= '0' && caracter <= '9') {
				cantidadCifras++;
			}
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);

		input.close();
	}
}
