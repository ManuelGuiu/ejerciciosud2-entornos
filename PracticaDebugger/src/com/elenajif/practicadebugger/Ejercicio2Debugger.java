package com.elenajif.practicadebugger;

import java.util.Scanner;

public class Ejercicio2Debugger {

	public static void main(String[] args) {
		/*
		 * Establecer un breakpoint en la primera instruccion y avanzar instruccion a
		 * instruccion (step into) analizando el contenido de las variables
		 */

		Scanner lector;
		int numeroLeido;
		int resultadoDivision;

		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		/*
		 * El error en este ejercicio sucede por el mismo motivo que en el anterior, por
		 * la condicion del for, en este caso seguira hasta que la i valga 0, sin
		 * embargo cuando sucede esto, en la division de la linea siguiente se produce
		 * un error ya que divide un numero entre 0 y java no puede realizar dicha
		 * operacion. La solucion seria dejar el bucle for de la siguiente manera:
		 * for(int i = numeroLeido; i > 0 ; i--)
		 */

		for (int i = numeroLeido; i >= 0; i--) {
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}

		lector.close();
	}

}
